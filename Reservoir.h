#ifndef Reservoir_h
#define Reservoir_h
#include "Pompe.h"

#include <iostream>

class Reservoir{

	private:
		bool etat_reservoir; // true = plein et false = vide
		int quantite;
		int capacite;
		Pompe p1;
		Pompe p2;

	public:
		Reservoir(int cap);
		bool getEtatReservoir();
		int getQuantite();
		int getCapacite();

		void setEtatReservoir(bool a);
		void setQuantite(int b);
		void verifQuantite();
		~Reservoir();
};
#endif