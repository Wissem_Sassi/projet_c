#ifndef Moteur_h
#define Moteur_h
#include "Reservoir.h"

#include <iostream>

class Moteur{

	private:
		Reservoir r;

	public:
		Moteur();
		Moteur(int c);
		~Moteur();
};
#endif