#include "Pompe.h"

using namespace std;

Pompe::Pompe(bool first){
	principale = first;
	if (principale)
	{
		est_marche = true;
	}
	else{
		est_marche = false;
	}
	est_panne = false;
	cout << "Creation de la Pompe " << endl;
};

bool Pompe::getMarche(){
	return this -> est_marche;
};

void Pompe::setMarche(bool a){
	est_marche = a;
};

bool Pompe::getPanne(){
	return this -> est_panne;
};



void Pompe::setPanne(bool b){
	est_panne = b;
};

bool Pompe::getPrincipale(){
	return this -> principale;
}

void Pompe::demarrer(){
	est_marche = true;
};

void Pompe::arreter(){
	est_marche = false;
};

void Pompe::inverseEtatP(){
	if(getMarche())
		this->arreter();
	else
		this->demarrer();
}

Pompe::~Pompe(){
	cout << "Destruction de la Pompe " << endl;
};
