#include "Vanne.h"

using namespace std;

Vanne::Vanne(int type_v){
	etat_vanne = false;
	type_vanne = type_v;
	cout << "Creation de la Vanne " << endl;
}

bool Vanne::getEtatVanne() {
	return this -> etat_vanne;
}

void Vanne::setEtatVanne(bool a) {
	etat_vanne = a;
}

int Vanne::getTypeVanne(){
	return this -> type_vanne;
}

void Vanne::ouverture(){
	if(!etat_vanne){
		cout << "vanne deja ouverte"<< endl;
	}
	else{
		setEtatVanne(false);
	}
}

void Vanne::fermeture(){
	if(etat_vanne){
		cout << "vanne deja fermée"<< endl;
	}
	else{
		setEtatVanne(true);
	}
}

void Vanne::inverseEtat(){
	if(getEtatVanne()){
		this->ouverture();
	}
	else{
		this->fermeture();
	}
}

Vanne::~Vanne(){
	cout << "Destruction de la Vanne " << endl;
}
