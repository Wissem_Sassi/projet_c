#ifndef Vanne_h
#define Vanne_h

#include <iostream>

class Vanne{

	private:
		bool etat_vanne; // Vanne ouverte == false ( Pas de circulation de carburant)
		int type_vanne; // 1 -> VT12 et VT23, 2 -> V12, V13, V23

	public:
		//constructeur
		Vanne(int type_v);
		//getter
		bool getEtatVanne();
		int getTypeVanne();
		//setter
		void setEtatVanne(bool a);
		//autre methode
		void ouverture();
		void fermeture();
		void inverseEtat();
		//destructeur
		~Vanne();
};
#endif