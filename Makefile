all: main
	./main

main: Main.o Reservoir.o Pompe.o Vanne.o Moteur.o
	g++ Main.o Reservoir.o Pompe.o Vanne.o Moteur.o -o main

Main.o: Main.cc Reservoir.h Pompe.h Vanne.h Moteur.h
	g++ Main.cc -c

Reservoir.o: Reservoir.cc Pompe.cc
	g++ Reservoir.cc Pompe.cc -c

Pompe.o: Pompe.cc
	g++ Pompe.cc -c

Vanne.o: Vanne.cc
	g++ Vanne.cc -c

Moteur.o: Moteur.cc Reservoir.cc
	g++ Moteur.cc Reservoir.cc -c

zip:
	tar -zcvf projet.tar.gz *.h *.cc Makefile

clean:
	rm -f *.o
	rm -f main
	rm -f projet.tar.gz
