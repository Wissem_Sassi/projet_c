#include "Moteur.h"

using namespace std;

Moteur::Moteur(int c) : r(c){
	cout << "Creation du Moteur " << endl;
}

Moteur::Moteur() : r(50){
	cout << "Creation du Moteur " << endl;
}

Moteur::~Moteur(){
	cout << "Destruction du Moteur " << endl;
}
