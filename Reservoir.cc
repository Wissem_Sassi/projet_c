#include "Reservoir.h"

using namespace std;

Reservoir::Reservoir(int cap): p1(true), p2(false){
	capacite = cap;
	quantite = capacite;
	etat_reservoir = true ;
	cout << "Creation du Reservoir " << endl;
}

bool Reservoir::getEtatReservoir() {
	return this -> etat_reservoir;
}

int Reservoir::getQuantite() {
	return this -> quantite;
}

int Reservoir::getCapacite() {
	return this -> capacite;
}

void Reservoir::setEtatReservoir(bool a) {
	etat_reservoir = a;
}

void Reservoir::setQuantite(int b) {
	quantite = b;
}

void Reservoir::verifQuantite(){
	if (quantite <= 0)
	{
		setEtatReservoir(false);
	}
	else{
		setEtatReservoir(true);
	}
}

Reservoir::~Reservoir(){
	cout << "Destruction du Reservoir " << endl;
}
