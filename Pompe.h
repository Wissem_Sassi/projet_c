#ifndef Pompe_h
#define Pompe_h

#include <iostream>

class Pompe{

	private:
		bool est_marche;
		bool est_panne;
		bool principale;

	public:
		//constructeur
		Pompe(bool first);
		//getter
		bool getMarche();
		bool getPanne();
		bool getPrincipale();
		
		//setter
		void setMarche(bool a);
		void setPanne(bool b);
		//autre methode
		void demarrer();
		void arreter();
		void inverseEtatP();
		//destructeur
		~Pompe();
};
#endif